package com.ulco.us;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * The type Background service.
 */
public class backgroundService extends Service {

    /**
     * The Runnable code.
     */
    public Runnable runnableCode;

    /**
     * The Handler.
     */
    Handler handler = new Handler();

    /**
     * The Reponse.
     */
    public String reponse;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Called every time a client starts the service.
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        runnableCode = new Runnable() {

            @Override
            public void run() {
                requestAgenda();
                handler.postDelayed(this, 2000);
            }
        };

        handler.post(runnableCode);
        return START_STICKY;
    }

    /*@Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnableCode);
        Toast.makeText(this, "Service destroyed by user", Toast.LENGTH_LONG).show();
    }*/

    /**
     * Will execute HTTP requests according to the class selected earlier.
     *
     * @return a @String containing the result of the HTTP requests
     */
    public String requestAgenda() {

        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (GeneralSecurityException e) {
        }
        createFile("main-ics.txt");
        createFile("class.txt");
        RequestQueue queue = Volley.newRequestQueue(this);

        String classe = readFile("class.txt").toString();
        String url = "";
        switch (classe) {
            case "[LP DIM FI]":
                url = "https://edt.univ-littoral.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?data=8241fc3873200214a9bb6791dfafa87ee0fa50826f0818af16cfc8af7aef7fd1906f45af276f59aec18424f8595af9f973866adc6bb17503";
                break;
            case "[LP DIM APP]":
                url = "https://edt.univ-littoral.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?data=8241fc38732002141c1a7ff6f3d825fae0fa50826f0818af16cfc8af7aef7fd1906f45af276f59aec18424f8595af9f973866adc6bb17503";
                break;
            case "[DUT1 TPA]":
                url = "https://edt.univ-littoral.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?data=8241fc38732002143ffcc28a30e8c25ae0fa50826f0818af16cfc8af7aef7fd1906f45af276f59aec18424f8595af9f973866adc6bb17503";
                break;
            case "[DUT1 TPB]":
                url = "https://edt.univ-littoral.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?data=8241fc3873200214633da5846189fc9de0fa50826f0818af16cfc8af7aef7fd1906f45af276f59aec18424f8595af9f973866adc6bb17503";
                break;
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        reponse = response;
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                reponse = error.getMessage();
            }
        });
        queue.add(stringRequest);
        processResponse(reponse);

        return reponse;
    }


    /**
     * Creates a file in the /Android/media/com.ulco.us/files/ folder.
     *
     * @param fileName the file name
     */
    public void createFile(String fileName) {
        try {
            File myDirectory = new File("/storage/emulated/0/Android/media/com.ulco.us/files/");
            File myFile = new File("/storage/emulated/0/Android/media/com.ulco.us/files/" + fileName);

            if (!myDirectory.exists()) {
                System.out.println(myDirectory.getName() + " directory don't exist.");
                if (myDirectory.mkdirs()) {
                    System.out.println(myDirectory.getName() + " directory created.");
                    if (myFile.createNewFile()) {
                        System.out.println("File created: " + myFile.getName());
                    } else {
                        System.out.println("File already exists.");
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("An error occurred when creating the file.");
            e.printStackTrace();
        }
    }

    /**
     * Delete a file.
     *
     * @param fileName the file name
     */
    public void fileDelete(String fileName) {
        File myObj = new File("/storage/emulated/0/Android/media/com.ulco.us/files/" + fileName);
        if (myObj.delete()) {
            System.out.println("Deleted the file: " + myObj.getName());
        } else {
            System.out.println("Failed to delete the file.");
        }
    }

    /**
     * Modifies a file.
     *
     * @param procResponse The Array you want to put inside the file
     * @param fileName     The file name
     * @param action       Action on the file
     */
    public void modifFile(ArrayList<String> procResponse, String fileName, String action) {
        if (action.equals("DELETE")) {
            ArrayList<String> tableauTraitement = new ArrayList<String>();
            String card = procResponse.toString();
            String file = readFile(fileName).toString();
            String cardFinal = card.substring(1, 60);

            String[] nbSummary = file.trim().split("SUMMARY");
            int SummaryNumber = nbSummary.length;
            System.out.println(SummaryNumber);
            if (SummaryNumber == 2) {
                String result = file.replace("" + cardFinal + "", "");
                result = result.replace("[", "").replace("]", "").replace(",", "");

                String[] strParts = result.split(" ");
                ArrayList<String> aList = new ArrayList<String>(Arrays.asList(strParts));

                for (String s : aList)
                    tableauTraitement.add(s);
                System.out.println(tableauTraitement);
                modifFile(tableauTraitement, fileName, "ADD");
            } else {
                System.out.println(file);

                file = file.replace("Z]", "Z, ]");
                String result = file.replace("" + cardFinal + ", ", "");
                result = result.replace("[", "").replace("]", "").replace(",", "");
                System.out.println(result);
                String[] strParts = result.split(" ");
                ArrayList<String> aList = new ArrayList<String>(Arrays.asList(strParts));

                for (String s : aList)
                    tableauTraitement.add(s);

                System.out.println(tableauTraitement);
                modifFile(tableauTraitement, fileName, "ADD");
            }
        }
        if (action.equals("ADD")) {
            try {
                FileWriter myWriter = new FileWriter("/storage/emulated/0/Android/media/com.ulco.us/files/" + fileName);
                for (String i : procResponse) {
                    myWriter.write(i + "\n");
                }
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            } catch (IOException e) {
                System.out.println("An error occurred when modifying the file.");
                e.printStackTrace();
            }
        }
        if (action.equals("ADD-SELECTION")) {

            ArrayList<String> tableauTraitement = new ArrayList<String>();

            String FileArchived = readFile("archived-ics.txt").toString();
            String FileCard = readFile("deleted-ics.txt").toString();
            String FileStarred = readFile("starred-ics.txt").toString();
            String FileIcs = readFile("main-ics.txt").toString();

            String ArchivedStarred = FileStarred + FileArchived + FileCard;

            String[] nbSummary = ArchivedStarred.trim().split("SUMMARY");
            int SummaryNumber = nbSummary.length;

            ArchivedStarred = ArchivedStarred.replace("[", "").replace("]", "").replace("ZS", "Z\nS").replace("Z, S", "Z\nS");

            if (SummaryNumber == 1) {

                String[] strParts = ArchivedStarred.split("\n");
                ArrayList<String> aList = new ArrayList<String>(Arrays.asList(strParts));

                for (String s : aList) {
                    FileIcs = FileIcs.replace("" + s + ", ", "");
                }
                FileIcs = FileIcs.replace("[", "").replace(",", "").replace("]", "").replace("ZE", "Z E").replace("S", " S").replaceFirst(" S", "S");

                String[] strPart2 = FileIcs.split(" ");
                ArrayList<String> bList = new ArrayList<String>(Arrays.asList(strPart2));

                for (String s : bList) {
                    tableauTraitement.add(s);
                }
                modifFile(tableauTraitement, "news-ics.txt", "ADD");

            } else {
                String[] strParts = ArchivedStarred.split("\n");
                ArrayList<String> aList = new ArrayList<String>(Arrays.asList(strParts));
                FileIcs = FileIcs.replace("Z]", "Z, ]");

                for (String s : aList) {
                    FileIcs = FileIcs.replace("" + s + ", ", "");
                }
                FileIcs = FileIcs.replace("[", "").replace(",", "").replace("]", "").replace("Z E", "Z  E").replace("S", " S").replaceFirst(" S", "S");

                String[] strPart2 = FileIcs.split("  ");
                ArrayList<String> bList = new ArrayList<String>(Arrays.asList(strPart2));

                for (String s : bList) {
                    tableauTraitement.add(s);
                }
                if (tableauTraitement.toString().contains("[]")) {
                    modifFile(tableauTraitement, "news-ics.txt", "ADD");
                } else {
                    modifFile(tableauTraitement, "news-ics.txt", "ADD");
                }
            }
        }
        if (action.equals("ADD-NO-OVERWRITE")) {
            ArrayList<String> tableauTraitement = new ArrayList<String>();

            String readFile = readFile(fileName).toString();
            String cardInfo = procResponse.toString();
            String FileCard = readFile + cardInfo;

            String result = FileCard.replace("[", "").replace("]", "").replace(",", "").replace("ZS", "Z S");
            String[] strParts = result.split(" ");

            for (String s : strParts)
                tableauTraitement.add(s);

            modifFile(tableauTraitement, fileName, "ADD");
        }
    }

    /**
     * Processes the result of the HTTP request to make it useable, then put it inside the file.
     *
     * @param response the HTTP response
     * @return string the processed HTTP response
     */
    public String processResponse(String response) {
        String test = response;
        if (test != null) {
            if (test.contains("SUMMARY")) {
                String[] nbSummary = test.trim().split("SUMMARY");

                ArrayList<String> procResponse = new ArrayList<String>();

                int SummaryNumber = nbSummary.length;
                System.out.println("Summary count is = " + SummaryNumber);

                Pattern ptSummary = Pattern.compile("SUMMARY...\\d\\d.");
                Matcher mtSummary = ptSummary.matcher(test);

                Pattern ptStart = Pattern.compile("START.\\d\\d\\d\\d\\d\\d\\d\\dT\\d\\d\\d\\d\\d\\dZ");
                Matcher mtStart = ptStart.matcher(test);

                Pattern ptEnd = Pattern.compile("END.\\d\\d\\d\\d\\d\\d\\d\\dT\\d\\d\\d\\d\\d\\dZ");
                Matcher mtEnd = ptEnd.matcher(test);

                for (int i = 0; i < SummaryNumber; i++) {
                    if (mtSummary.find() && mtStart.find() && mtEnd.find()) {
                        procResponse.add(mtSummary.group());
                        procResponse.add(mtStart.group());
                        procResponse.add(mtEnd.group());
                    }
                }

                if (procResponse.equals(readFile("main-ics.txt"))) {
                    System.out.println("No change needed");
                    createFile("news-ics.txt");
                    modifFile(procResponse, "news-ics.txt", "ADD-SELECTION");
                } else {
                    System.out.println("Fichier different donc a modif puis a notifié l'user");
                    createNotification();
                    modifFile(procResponse, "main-ics.txt", "ADD");
                    createFile("news-ics.txt");
                    modifFile(procResponse, "news-ics.txt", "ADD-SELECTION");
                }
            }
        } else {
            System.out.println("ics response empty");
        }

        return response;
    }

    /**
     * Will read a file.
     *
     * @param fileName the file name
     * @return the file content
     */
    public ArrayList readFile(String fileName) {
        ArrayList<String> fichierLecture = new ArrayList<String>();

        try {
            File myFile = new File("/storage/emulated/0/Android/media/com.ulco.us/files/" + fileName);
            Scanner myReader = new Scanner(myFile);

            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                fichierLecture.add(data);
            }
            System.out.println("End of reading");
            myReader.close();

        } catch (FileNotFoundException e) {
            System.out.println("An error occurred when reading the file.");
            e.printStackTrace();
        }
        return fichierLecture;

    }

    /**
     * Will generate a notification.
     */
    private void createNotification() {

        Intent targetIntent = new Intent(this, BroadcastNotify.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent HomeIntent = new Intent(this, MainActivity.class);
        PendingIntent contentHomeIntent = PendingIntent.getActivity(this, 0, HomeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this, "IUT")
                        .setSmallIcon(R.drawable.ic_notification)
                        .setColor(Color.BLUE)
                        .setContentTitle("ULCO Service")
                        .setContentText("Un changement dans l'emploi du temps a eu lieu")
                        .addAction(R.drawable.ic_notification, "Voir", contentHomeIntent);

        int NOTIFICATION_ID = 12345;
        builder.setContentIntent(contentIntent);
        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(NOTIFICATION_ID, builder.build());
    }
}


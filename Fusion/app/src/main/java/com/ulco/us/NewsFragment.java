package com.ulco.us;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import soup.neumorphism.NeumorphImageButton;

/**
 * The type Newsfragment.
 */
public class NewsFragment extends Fragment {

    private Context context;

    /**
     * Call context each time you need it
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    /**
     * The Service.
     */
    backgroundService service = new backgroundService();

    /**
     * The News view.
     */
    RecyclerView newsView;

    /**
     * The Recycler view adapter.
     */
    RecyclerViewAdapter recyclerViewAdapter;

    /**
     * The Carousel view.
     */
    ViewPager2 carouselView;

    /**
     * The Carousel adapter.
     */
    CarouselAdapter carouselAdapter;

    /**
     * Instantiates a new NewsFragment.
     */
    public NewsFragment() {
        //Required empty public constructor
    }

    /**
     * What happens when the fragment is created
     *
     * @param inflater           what it goes into
     * @param container
     * @param savedInstanceState the last state of the fragment
     * @return a view
     */
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        final View view = inflater.inflate(R.layout.fragment_news, container, false);

        ArrayList MyStarredList = service.readFile("starred-ics.txt");
        ArrayList myListNews = service.readFile("news-ics.txt");

        newsView = view.findViewById(R.id.newsRecycler);
        recyclerViewAdapter = new RecyclerViewAdapter(myListNews);
        newsView.setAdapter(recyclerViewAdapter);
        newsView.setNestedScrollingEnabled(false);
        Integer recyclerPopulation = recyclerViewAdapter.getItemCount();

        TextView lastNews = view.findViewById(R.id.news_lastTitle);
        String lastNewsTitle = lastNews.getText().toString();
        lastNews.setText(lastNewsTitle + " (" + recyclerPopulation + ")");

        SharedPreferences newsViewPreferences = getActivity().getSharedPreferences("population", 0);
        SharedPreferences.Editor preferencesEditor = newsViewPreferences.edit();

        preferencesEditor.putInt("newsData", recyclerPopulation);
        preferencesEditor.commit();

        carouselView = view.findViewById(R.id.starredCarousel);
        carouselView.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        carouselView.setOffscreenPageLimit(3);

        carouselAdapter = new CarouselAdapter(MyStarredList);
        carouselView.setAdapter(carouselAdapter);

        final float pageMargin = getResources().getDimensionPixelOffset(R.dimen.pageMargin);
        final float pageOffset = getResources().getDimensionPixelOffset(R.dimen.offset);

        /**
         * sets the page transformer so the carousel is correctly sized
         */
        carouselView.setPageTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float myOffset = position * -(2 * pageOffset + pageMargin);
                if (carouselView.getOrientation() == ViewPager2.ORIENTATION_HORIZONTAL) {
                    if (ViewCompat.getLayoutDirection(carouselView) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                        page.setTranslationX(-myOffset);
                    } else {
                        page.setTranslationX(myOffset);
                    }
                } else {
                    page.setTranslationY(myOffset);
                }
            }
        });

        NeumorphImageButton infoButton = view.findViewById(R.id.info);

        infoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context)
                        .setTitle("Guide")
                        .setMessage("- Mes dernières nouvelles :" +
                                "\n \n\ud83d\udccc Glisser à droite pour mise en favoris." +
                                "\n\ud83d\uddc3 Glisser à gauche pour archivage." +
                                "\n \n- Alertes en favoris et archives :" +
                                "\n \n\u274c Appui long pour suppression définitive.")
                        .setPositiveButton("Fermer", null);
                AlertDialog alertDialog = builder.create();
                alertDialog.setOnShowListener(dialog -> {
                    TextView message = alertDialog.findViewById(android.R.id.message);
                    if (message != null) {
                        final Typeface typeface = ResourcesCompat.getFont(context, R.font.quicksand);
                        message.setTypeface(typeface);
                    }
                });
                alertDialog.show();
            }
        });

        /**
         * Left swipe on NewsRecycler item
         */
        ItemTouchHelper.SimpleCallback archiveItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            /**
             * What happens when you move the item
             * @param recyclerView
             * @param viewHolder
             * @param target
             * @return
             */
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            /**
             * What happens when you swipe the item
             *
             * @param viewHolder
             * @param swipeDir the swipe direction
             */
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                Toast.makeText(view.getContext(), "Nouvelle archivée", Toast.LENGTH_SHORT).show();
                service.createFile("archived-ics.txt");
                //Remove item and notify
                int position = viewHolder.getAdapterPosition();
                //récuperation des données de la card qu'on swipe
                String starredSummary = ((TextView) newsView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.summaryTextView)).getText().toString();
                String starredStart = ((TextView) newsView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.startTextView)).getText().toString();
                String starredEnd = ((TextView) newsView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.endTextView)).getText().toString();

                //formatage des données pour quelle correspondes à celle du newsics.txt
                String heureDebut = starredStart.substring(55, 60);
                String heureFin = starredEnd.substring(18, 23);
                String ddmmyyyy = starredEnd.substring(25, 35);
                //replace les trucs de merde
                String bonStart = heureDebut.replace(":", "");
                String bonEnd = heureFin.replace(":", "");
                String ddmmyyyy2 = ddmmyyyy.replace("/", "");
                //mise en forme date
                String dd = ddmmyyyy2.substring(0, 2);
                String mm = ddmmyyyy2.substring(2, 4);
                String yyyy = ddmmyyyy2.substring(4, 8);
                String bonDate = yyyy + mm + dd;

                //tableau a envoyé dans modif
                ArrayList<String> favori = new ArrayList<String>();
                favori.add("SUMMARY:" + starredSummary);
                favori.add("START:" + bonDate + "T" + bonStart + "00Z");
                favori.add("END:" + bonDate + "T" + bonEnd + "00Z");

                service.modifFile(favori, "archived-ics.txt", "ADD-NO-OVERWRITE");
                service.modifFile(favori, "news-ics.txt", "DELETE");

                //newsList.remove(position);
                recyclerViewAdapter.notifyItemRemoved(position);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        refreshNews();
                    }
                }, 500);
            }
        };

        ItemTouchHelper archiveTouchHelper = new ItemTouchHelper(archiveItemTouchCallback);
        archiveTouchHelper.attachToRecyclerView(newsView);

        /**
         * Right swipe on NewsRecycler item
         */
        ItemTouchHelper.SimpleCallback starredItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

            /**
             * When swiping on the right, put items in favorites
             *
             * @param recyclerView
             * @param viewHolder
             * @param target
             * @return
             */

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                Toast.makeText(view.getContext(), "Nouvelle épinglée", Toast.LENGTH_SHORT).show();
                //Remove item and notify
                int position = viewHolder.getAdapterPosition();
                service.createFile("starred-ics.txt");

                //récuperation des données de la card qu'on swipe
                String starredSummary = ((TextView) newsView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.summaryTextView)).getText().toString();
                String starredStart = ((TextView) newsView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.startTextView)).getText().toString();
                String starredEnd = ((TextView) newsView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.endTextView)).getText().toString();

                //formatage des données pour quelle correspondes à celle du newsics.txt
                String heureDebut = starredStart.substring(55, 60);
                String heureFin = starredEnd.substring(18, 23);
                String mois = starredEnd.substring(25, 35);
                //replace les trucs de merde
                String bonStart = heureDebut.replace(":", "");
                String bonEnd = heureFin.replace(":", "");
                String bonMois = mois.replace("/", "");
                //mise en forme date
                String dd = bonMois.substring(0, 2);
                String mm = bonMois.substring(2, 4);
                String yyyy = bonMois.substring(4, 8);
                String bonDate = yyyy + mm + dd;

                //tableau a envoyé dans modif
                ArrayList<String> favori = new ArrayList<String>();
                favori.add("SUMMARY:" + starredSummary);
                favori.add("START:" + bonDate + "T" + bonStart + "00Z");
                favori.add("END:" + bonDate + "T" + bonEnd + "00Z");

                service.modifFile(favori, "starred-ics.txt", "ADD-NO-OVERWRITE");
                service.modifFile(favori, "news-ics.txt", "DELETE");
                //newsList.remove(position);
                recyclerViewAdapter.notifyItemRemoved(position);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        refreshNews();
                    }
                }, 500);
            }
        };

        ItemTouchHelper starredTouchHelper = new ItemTouchHelper(starredItemTouchCallback);
        starredTouchHelper.attachToRecyclerView(newsView);

        carouselAdapter.setOnItemListener(new CarouselAdapter.OnItemListener() {

            /**
             * On long click on the RecyclerView item
             *
             * @param viewHolder
             * @param position
             */
            @Override
            public void OnItemLongClickListener(RecyclerView.ViewHolder viewHolder, int position) {
                Toast.makeText(getContext(),
                        "Nouvelle épinglée supprimée",
                        Toast.LENGTH_SHORT).show();
                service.createFile("deleted-ics.txt");
                String starredSummary = ((TextView) viewHolder.itemView.findViewById(R.id.summaryTextView)).getText().toString();
                String starredStart = ((TextView) viewHolder.itemView.findViewById(R.id.startTextView)).getText().toString();
                String starredEnd = ((TextView) viewHolder.itemView.findViewById(R.id.endTextView)).getText().toString();

                //formatage des données pour quelle correspondes à celle du newsics.txt
                String heureDebut = starredStart.substring(55, 60);
                String heureFin = starredEnd.substring(18, 23);
                String ddmmyyyy = starredEnd.substring(25, 35);
                //replace les trucs de merde
                String bonStart = heureDebut.replace(":", "");
                String bonEnd = heureFin.replace(":", "");
                String ddmmyyyy2 = ddmmyyyy.replace("/", "");
                //mise en forme date
                String dd = ddmmyyyy2.substring(0, 2);
                String mm = ddmmyyyy2.substring(2, 4);
                String yyyy = ddmmyyyy2.substring(4, 8);
                String bonDate = yyyy + mm + dd;

                //tableau a envoyé dans modif
                ArrayList<String> favori = new ArrayList<String>();
                favori.add("SUMMARY:" + starredSummary);
                favori.add("START:" + bonDate + "T" + bonStart + "00Z");
                favori.add("END:" + bonDate + "T" + bonEnd + "00Z");

                service.modifFile(favori, "deleted-ics.txt", "ADD-NO-OVERWRITE");
                service.modifFile(favori, "starred-ics.txt", "DELETE");

                recyclerViewAdapter.notifyItemRemoved(position);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        refreshNews();
                    }
                }, 500);
            }
        });

        return view;
    }

    /**
     * Refresh fragment
     */
    public void refreshNews() {
        getFragmentManager().beginTransaction().replace(R.id.container, new NewsFragment()).commit();
    }
}
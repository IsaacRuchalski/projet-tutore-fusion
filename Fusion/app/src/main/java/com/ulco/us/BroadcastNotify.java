package com.ulco.us;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * A notifier.
 *
 * @deprecated
 */
public class BroadcastNotify extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

    }
}

package com.ulco.us;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import soup.neumorphism.NeumorphCardView;
import soup.neumorphism.NeumorphImageButton;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFragment extends Fragment implements View.OnClickListener {

    private Context context;

    /**
     * Call context each time you need it
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    /**
     * Services RelativeLayout which contain CardViews
     */
    private RelativeLayout servicesA;
    private RelativeLayout servicesB;

    /**
     * RelativeLayouts childs
     */
    NeumorphCardView ncwAsA, ncwBsA, ncwCsA, ncwDsA,
            ncwAsB, ncwBsB, ncwCsB, ncwDsB;

    /**
     * Instantiates a new Services fragment.
     */
    public ServicesFragment() {
        //Required empty public constructor
    }

    /**
     * when you create the fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_services, container, false);

        NeumorphImageButton external = view.findViewById(R.id.external);
        external.setOnClickListener(this);

        NeumorphCardView btnServicesA = view.findViewById(R.id.btnServicesA);
        btnServicesA.setOnClickListener(listenerServicesA);
        NeumorphCardView btnServicesB = view.findViewById(R.id.btnServicesB);
        btnServicesB.setOnClickListener(listenerServicesB);

        servicesA = view.findViewById(R.id.servicesA);
        servicesB = view.findViewById(R.id.servicesB);

        NeumorphImageButton btnAsA = view.findViewById(R.id.btnAsA);
        btnAsA.setOnClickListener(this);
        NeumorphImageButton btnBsA = view.findViewById(R.id.btnBsA);
        btnBsA.setOnClickListener(this);
        NeumorphImageButton btnCsA = view.findViewById(R.id.btnCsA);
        btnCsA.setOnClickListener(this);
        NeumorphImageButton btnDcA = view.findViewById(R.id.btnDsA);
        btnDcA.setOnClickListener(this);

        NeumorphImageButton btnAsB = view.findViewById(R.id.btnAsB);
        btnAsB.setOnClickListener(this);
        NeumorphImageButton btnBsB = view.findViewById(R.id.btnBsB);
        btnBsB.setOnClickListener(this);
        NeumorphImageButton btnCsB = view.findViewById(R.id.btnCsB);
        btnCsB.setOnClickListener(this);
        NeumorphImageButton btnDsB = view.findViewById(R.id.btnDsB);
        btnDsB.setOnClickListener(this);

        ncwAsA = view.findViewById(R.id.ncwAsA);
        ncwBsA = view.findViewById(R.id.ncwBsA);
        ncwCsA = view.findViewById(R.id.ncwCsA);
        ncwDsA = view.findViewById(R.id.ncwDsA);

        ncwAsB = view.findViewById(R.id.ncwAsB);
        ncwBsB = view.findViewById(R.id.ncwBsB);
        ncwCsB = view.findViewById(R.id.ncwCsB);
        ncwDsB = view.findViewById(R.id.ncwDsB);

        return view;
    }

    /**
     * Show and hide ServicesA RelativeLayout
     */
    private final View.OnClickListener listenerServicesA = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            switch (servicesA.getVisibility()) {
                case View.VISIBLE:
                    servicesA.setVisibility(View.VISIBLE);
                    break;
                case View.INVISIBLE:
                    servicesB.setVisibility(View.INVISIBLE);
                    animationServiceA();
                    servicesA.setVisibility(View.VISIBLE);
                    break;
                case View.GONE:
                    break;
            }
        }
    };

    /**
     * Show and hide ServicesB RelativeLayout
     */
    private final View.OnClickListener listenerServicesB = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (servicesB.getVisibility()) {
                case View.VISIBLE:
                    servicesB.setVisibility(View.VISIBLE);
                    break;
                case View.INVISIBLE:
                    servicesA.setVisibility(View.INVISIBLE);
                    animationServiceB();
                    servicesB.setVisibility(View.VISIBLE);
                    break;
                case View.GONE:
                    break;
            }
        }
    };

    /**
     * onClick redirect to a website
     *
     * @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.external:
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://extra.univ-littoral.fr/"));
                startActivity(viewIntent);
                break;
            case R.id.btnAsA:
                Intent viewAIntentA =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://edt.univ-littoral.fr/public/iut-info"));
                startActivity(viewAIntentA);
                break;
            case R.id.btnBsA:
                Intent viewBIntentA =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://iic0e.univ-littoral.fr/moodle/"));
                startActivity(viewBIntentA);
                break;
            case R.id.btnCsA:
                Intent viewCIntentA =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://sakai.univ-littoral.fr/"));
                startActivity(viewCIntentA);
                break;
            case R.id.btnDsA:
                Intent viewDIntentA =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://geiicalais.univ-littoral.fr/"));
                startActivity(viewDIntentA);
                break;
            case R.id.btnAsB:
                Intent viewAIntentB =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://twitter.com/IUT_Littoral"));
                startActivity(viewAIntentB);
                break;
            case R.id.btnBsB:
                Intent viewBIntentB =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://www.facebook.com/IUTLittoral/"));
                startActivity(viewBIntentB);
                break;
            case R.id.btnCsB:
                Intent viewCIntentB =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://www.instagram.com/ulco.univ/"));
                startActivity(viewCIntentB);
                break;
            case R.id.btnDsB:
                Intent viewDIntentB =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://discord.com/"));
                startActivity(viewDIntentB);
                break;
            default:
                break;
        }
    }

    /**
     * Animate ServiceA RelativeLayout CardViews
     */
    public void animationServiceA() {
        View[] sA = new View[]{ncwAsA, ncwBsA, ncwCsA, ncwDsA};
        long delayBetweenAnimations = 25L;

        for (int i = 0; i < sA.length; i++) {
            final View view = sA[i];
            long delay = i * delayBetweenAnimations;

            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    LayoutAnimationController layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.carousel_layout);
                    layoutAnimation.start();
                }
            }, delay);
        }
    }

    /**
     * Animate ServiceB RelativeLayout CardViews
     */
    public void animationServiceB() {
        View[] sB = new View[]{ncwAsB, ncwBsB, ncwCsB, ncwDsB};
        long delayBetweenAnimations = 25L;

        for (int i = 0; i < sB.length; i++) {
            final View view = sB[i];
            long delay = i * delayBetweenAnimations;

            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    LayoutAnimationController layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.carousel_layout);
                    layoutAnimation.start();
                }
            }, delay);
        }
    }
}
package com.ulco.us;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import soup.neumorphism.NeumorphCardView;

/**
 * The type Settings fragment.
 */
public class SettingsFragment extends Fragment {
    /**
     * The Service.
     */
    backgroundService service = new backgroundService();

    /**
     * The List class.
     */
    String[] listClass;

    /**
     * The Text class.
     */
    TextView text_Class;

    /**
     * The Archived view.
     */
    RecyclerView archivedView;

    /**
     * The Recycler view adapter.
     */
    RecyclerViewAdapter recyclerViewAdapter;

    /**
     * Instantiates a new Settings fragment.
     */
    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * when creating the view
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        NeumorphCardView switchClass = view.findViewById(R.id.cdClass);
        text_Class = view.findViewById(R.id.tvClassDefault);
        //Store the content of the file in ArrayList
        ArrayList reponse = service.readFile("class.txt");

        //Initialization of the int k which contains the length of the file
        int longueurFichier = reponse.toString().length();

        //If file is empty the answer is [] therefore 2 characters, the student has not chosen his class
        if (longueurFichier == 2) {
            text_Class.setText("Choisir une classe");
        } else
        //If the content of the file is greater than 2 characters, this displays the class is transformed [class] -> class
        {
            ArrayList maClass = service.readFile("class.txt");
            int longueur = maClass.toString().length();
            String maBonneClass = maClass.toString().substring(1, longueur - 1);
            text_Class.setText(maBonneClass);
        }

        /**
         * when clicking on the switchClass
         */
        switchClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listClass = new String[]{"DUT1 TPA", "DUT1 TPB", "LP DIM FI", "LP DIM APP"};
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                mBuilder.setTitle("Choisir une class");
                mBuilder.setSingleChoiceItems(listClass, -1, new DialogInterface.OnClickListener() {
                    /**
                     * clicking when selecting class
                     * @param dialogInterface
                     * @param i
                     */
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        text_Class.setText(listClass[i]);
                        ArrayList<String> reponseTraitement = new ArrayList<>();
                        reponseTraitement.add(listClass[i]);
                        if (service.readFile("class.txt").contains("[]")) {
                            System.out.println("No file to delete");
                            service.modifFile(reponseTraitement, "class.txt", "ADD");
                        } else if (service.readFile("class.txt").toString().contains("[" + listClass[i] + "]")) {
                            System.out.println("Same class");
                        } else {
                            System.out.println("File to delete");
                            service.fileDelete("main-ics.txt");
                            service.fileDelete("news-ics.txt");
                            service.fileDelete("starred-ics.txt");
                            service.fileDelete("archived-ics.txt");
                            service.fileDelete("deleted-ics.txt");

                            service.modifFile(reponseTraitement, "class.txt", "ADD");
                        }
                    }

                });
                mBuilder.setNeutralButton("Fermer", new DialogInterface.OnClickListener() {
                    /**
                     * when returning while selecting class
                     * @param dialogInterface
                     * @param i
                     */
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(view.getContext(), "Récupération des dernières nouvelles ...", Toast.LENGTH_SHORT).show();

                    }
                });
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });

        backgroundService service = new backgroundService();
        ArrayList myListArchive = service.readFile("archived-ics.txt");

        archivedView = view.findViewById(R.id.archivedRecycler);
        archivedView.setNestedScrollingEnabled(false);
        recyclerViewAdapter = new RecyclerViewAdapter(myListArchive);
        archivedView.setAdapter(recyclerViewAdapter);

        int recyclerPopulation = recyclerViewAdapter.getItemCount();

        TextView lastNews = view.findViewById(R.id.settings_archivedTitle);
        String lastNewsTitle = lastNews.getText().toString();
        lastNews.setText(lastNewsTitle + " (" + recyclerPopulation + ")");

        recyclerViewAdapter.setOnItemListener(new RecyclerViewAdapter.OnItemListener() {

            /**
             * On long click on the RecyclerView item
             *
             * @param view
             * @param position
             */
            @Override
            public void OnItemLongClickListener(View view, int position) {
                Toast.makeText(getContext(),
                        "Nouvelle archivée supprimée",
                        Toast.LENGTH_SHORT).show();
                service.createFile("deleted-ics.txt");
                //Hop hop la deuxième méthode ici j'ai pas ton temps
                String starredSummary = ((TextView) archivedView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.summaryTextView)).getText().toString();
                String starredStart = ((TextView) archivedView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.startTextView)).getText().toString();
                String starredEnd = ((TextView) archivedView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.endTextView)).getText().toString();

                //formatage des données pour quelle correspondes à celle du newsics.txt
                String heureDebut = starredStart.substring(55, 60);
                String heureFin = starredEnd.substring(18, 23);
                String ddmmyyyy = starredEnd.substring(25, 35);
                //replace les trucs de merde
                String bonStart = heureDebut.replace(":", "");
                String bonEnd = heureFin.replace(":", "");
                String ddmmyyyy2 = ddmmyyyy.replace("/", "");
                //mise en forme date
                String dd = ddmmyyyy2.substring(0, 2);
                String mm = ddmmyyyy2.substring(2, 4);
                String yyyy = ddmmyyyy2.substring(4, 8);
                String bonDate = yyyy + mm + dd;

                //tableau a envoyé dans modif
                ArrayList<String> favori = new ArrayList<String>();
                favori.add("SUMMARY:" + starredSummary);
                favori.add("START:" + bonDate + "T" + bonStart + "00Z");
                favori.add("END:" + bonDate + "T" + bonEnd + "00Z");

                service.modifFile(favori, "deleted-ics.txt", "ADD-NO-OVERWRITE");
                service.modifFile(favori, "archived-ics.txt", "DELETE");

                recyclerViewAdapter.notifyItemRemoved(position);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        refreshSettings();
                    }
                }, 500);
            }
        });

        return view;
    }

    /**
     * Refresh fragment
     */
    public void refreshSettings() {
        getFragmentManager().beginTransaction().replace(R.id.container, new SettingsFragment()).commit();
    }
}
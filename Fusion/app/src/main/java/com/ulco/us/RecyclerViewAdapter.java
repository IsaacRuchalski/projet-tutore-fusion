package com.ulco.us;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * The type Recycler view adapter.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private OnItemListener onDeletedItemListener;

    /**
     * The News list.
     */
    List<String> newsList;

    /**
     * Instantiates a new Recycler view adapter.
     *
     * @param newsList the news list
     */
    public RecyclerViewAdapter(List<String> newsList) {
        this.newsList = newsList;
    }

    /**
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
     *
     * @param parent
     * @param viewType
     * @return a view holder
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.news_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnLongClickListener(new DeletedItemListener());

        return viewHolder;
    }

    /**
     * when binded to a view Holder
     *
     * @param holder   the view holder
     * @param position the position of the view holder
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        position = position * 3;

        //Original format
        DateFormat originalFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.FRANCE);

        //Target formats
        DateFormat targetStartFormat = new SimpleDateFormat("HH:mm");
        DateFormat targetEndFormat = new SimpleDateFormat("HH:mm (dd/MM/yyyy)");

        try {
            Date startDate = originalFormat.parse(newsList.get(position + 1).substring(6, 21));
            Date endDate = originalFormat.parse(newsList.get(position + 2).substring(4, 19));

            String startPreview = "Un cours a été ajouté à votre planning, il est prévu à ";
            String formattedStartDate = targetStartFormat.format(startDate);

            String endPreview = "Il se terminera à ";
            String formattedEndDate = targetEndFormat.format(endDate);

            holder.newsStart.setText(startPreview + formattedStartDate + ".");
            holder.newsEnd.setText(endPreview + formattedEndDate + ".");

        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.newsSummary.setText(newsList.get(position).substring(8, 13));
        holder.itemView.setId(position / 3);
    }

    /**
     * get count of items
     *
     * @return count of items
     */
    @Override
    public int getItemCount() {
        return newsList.size() / 3;
    }

    /**
     * The type View holder.
     */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * The News summary.
         */
        TextView newsSummary,

        /**
         * The News start.
         */
        newsStart,

        /**
         * The News end.
         */
        newsEnd;

        /**
         * Instantiates a new View holder.
         *
         * @param itemView the item view
         */
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            newsSummary = itemView.findViewById(R.id.summaryTextView);
            newsStart = itemView.findViewById(R.id.startTextView);
            newsEnd = itemView.findViewById(R.id.endTextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        }
    }

    //Listening interface class
    public interface OnItemListener {
        void OnItemLongClickListener(View view, int position);
    }

    //Implement the interface callback method to transfer the click event to the external caller
    class DeletedItemListener implements View.OnLongClickListener {

        @Override
        public boolean onLongClick(View view) {
            if (onDeletedItemListener != null) {
                onDeletedItemListener.OnItemLongClickListener(view, view.getId());
            }
            return true;
        }
    }

    //Instantiate the exposed caller and define the Listener's method ()
    public void setOnItemListener(OnItemListener listener) {
        this.onDeletedItemListener = listener;
    }
}

package com.ulco.us;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * The type Carousel adapter.
 */
public class CarouselAdapter extends RecyclerView.Adapter<CarouselAdapter.MyViewHolder> {

    /**
     * OnItemListener for long press
     */
    private OnItemListener onStarredItemListener;

    /**
     * The News list.
     */
    List<String> newsList;

    /**
     * Instantiates a new Carousel adapter.
     *
     * @param newsList the news list
     */
    public CarouselAdapter(List<String> newsList) {
        this.newsList = newsList;
    }

    /**
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.starred_item, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view);

        //Add a click event for each item in onCreateViewHolder()
        view.setOnLongClickListener(new StarredItemListener());

        return viewHolder;
    }

    /**
     * formats text when entering the carousel.
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        position = position * 3;

        //Original format
        DateFormat originalFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.FRANCE);

        //Target formats
        DateFormat targetStartFormat = new SimpleDateFormat("HH:mm");
        DateFormat targetEndFormat = new SimpleDateFormat("HH:mm (dd/MM/yyyy)");

        try {
            Date startDate = originalFormat.parse(newsList.get(position + 1).substring(6, 21));
            Date endDate = originalFormat.parse(newsList.get(position + 2).substring(4, 19));

            String startPreview = "Un cours a été ajouté à votre planning, il est prévu à ";
            String formattedStartDate = targetStartFormat.format(startDate);

            String endPreview = "Il se terminera à ";
            String formattedEndDate = targetEndFormat.format(endDate);

            holder.newsStart.setText(startPreview + formattedStartDate + ".");
            holder.newsEnd.setText(endPreview + formattedEndDate + ".");

        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.newsSummary.setText(newsList.get(position).substring(8, 13));

        int recyclerPopulation = (position / 3);
        String myStarredTitle = holder.starredTitle.getText().toString();
        holder.starredTitle.setText(myStarredTitle + " " + recyclerPopulation);

        holder.itemView.setId(position / 3);

    }

    /**
     * Gets the item count.
     *
     * @return number of items
     */
    @Override
    public int getItemCount() {
        return newsList.size() / 3;
    }

    /**
     * Describes an item view and metadata about its place within the RecyclerView.
     */
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * The News summary of items.
         */
        TextView newsSummary,

        /**
         * The News start.
         */
        newsStart,

        /**
         * The News end.
         */
        newsEnd,

        /**
         * The starred title.
         */
        starredTitle;

        /**
         * Instantiates a new MyView holder.
         *
         * @param itemView the item view
         */
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            newsSummary = itemView.findViewById(R.id.summaryTextView);
            newsStart = itemView.findViewById(R.id.startTextView);
            newsEnd = itemView.findViewById(R.id.endTextView);
            starredTitle = itemView.findViewById(R.id.starredTitleTextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        }
    }

    //Listening interface class
    public interface OnItemListener {
        void OnItemLongClickListener(RecyclerView.ViewHolder viewHolder, int position);
    }

    //Implement the interface callback method to transfer the click event to the external caller
    class StarredItemListener implements View.OnLongClickListener {

        @Override
        public boolean onLongClick(View view) {
            if (onStarredItemListener != null) {
                onStarredItemListener.OnItemLongClickListener(new MyViewHolder(view), view.getId());
            }
            return true;
        }
    }

    //Instantiate the exposed caller and define the Listener's method ()
    public void setOnItemListener(OnItemListener listener) {
        this.onStarredItemListener = listener;
    }
}


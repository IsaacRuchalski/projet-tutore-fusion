package com.ulco.us;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import nl.joery.animatedbottombar.AnimatedBottomBar;

/**
 * The main activity.
 */
public class MainActivity extends AppCompatActivity {

    private static final int STORAGE_PERMISSION_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        boolean firstStart = prefs.getBoolean("firstStart", true);
        if (firstStart) {
            showStartDialog();
        }

        startService(new Intent(MainActivity.this, backgroundService.class));
        createNotificationChannel();
        AnimatedBottomBar myBottomBar = findViewById(R.id.bottom_bar);
        SharedPreferences preferences = getSharedPreferences("population", 0);
        Integer currentNewsNumber = preferences.getInt("newsData", 0);
        String currentNewsString = Integer.toString(currentNewsNumber);

        myBottomBar.setBadgeAtTabIndex(0, new AnimatedBottomBar.Badge(
                currentNewsString,
                getResources().getColor(R.color.charcoal_color),
                Color.WHITE,
                20));

        if (currentNewsNumber == 0) {
            myBottomBar.clearBadgeAtTabIndex(0);
        }

        if (savedInstanceState == null) {
            myBottomBar.selectTabById(R.id.news, true);
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new NewsFragment()).commit();
        }

        /**
         * The fragment selector, a bottom navigation bar.
         */
        myBottomBar.setOnTabSelectListener(new AnimatedBottomBar.OnTabSelectListener() {
            @Override
            public void onTabSelected(int lastIndex, @Nullable AnimatedBottomBar.Tab lastTab, int newIndex, @NotNull AnimatedBottomBar.Tab newTab) {
                Fragment fragment = null;
                switch (newTab.getId()) {
                    case R.id.news:
                        fragment = new NewsFragment();
                        break;

                    case R.id.services:
                        fragment = new ServicesFragment();
                        break;

                    case R.id.settings:
                        fragment = new SettingsFragment();
                        break;
                }
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            /*.setCustomAnimations(
                                    R.anim.move_from_right,
                                    R.anim.move_to_left,
                                    R.anim.move_from_left,
                                    R.anim.move_to_right)*/.replace(R.id.container, fragment).commit();
                }
            }

            @Override
            public void onTabReselected(int i, @NotNull AnimatedBottomBar.Tab tab) {

            }
        });
    }

    /**
     * Creates a notification channel for further use of notifications. Don't be mad at me, it's android's fault !
     */
    private void createNotificationChannel() {
        /* Create the NotificationChannel, but only on API 26+ because the NotificationChannel
        class is new and not in the support library */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "IUT";
            String description = "Canal pour l'application ULCO Service";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("IUT", name, importance);
            channel.setDescription(description);
            /* Register the channel with the system, you can't change the importance
            or other notification behaviors after this */
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     * Dialog first launch
     */
    private void showStartDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Bienvenue sur ULCO Service !")
                .setMessage("Un outil utile au quotidien à l'IUT du Littoral Côte d'Opale. \ud83d\udcbc" +
                        "\n \n- Restez informés sur les changements d'emploi du temps grâce aux notifications" +
                        "\n- Accédez aux services universitaires mis à votre disposition plus rapidement" +
                        "\n- Gagnez du temps sur votre organisation" +
                        "\n \nPour commencer, choisissez votre classe dans vos paramètres :")
                .setPositiveButton("Mes paramètres", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkPermission(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);
                        dialog.dismiss();
                        Fragment someFragment = new SettingsFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, someFragment).commit();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(dialog -> {
            TextView message = alertDialog.findViewById(android.R.id.message);
            if (message != null) {
                final Typeface typeface = ResourcesCompat.getFont(this, R.font.quicksand);
                message.setTypeface(typeface);
            }
        });
        alertDialog.show();
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("firstStart", false);

        System.out.println("premier lancement");
        editor.apply();
    }

    /**
     * To check and request permission.
     *
     * @param permission
     * @param requestCode
     */
    public void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission)
                == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{permission},
                    requestCode);
        } else {
            Toast.makeText(MainActivity.this,
                    "Permission already granted",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    /**
     * Manage user response to permission.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this,
                        "Autorisation de stockage accordée",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(MainActivity.this,
                        "Autorisation de stockage refusée",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}


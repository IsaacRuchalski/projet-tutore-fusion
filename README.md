<div align="center">
  <a href="#">
    <img alt="Open Source" src="https://badgen.net/badge/Open%20Source/Yes/green?icon=github" />
  </a>
  <a href="#">
    <img alt="Language" src="https://img.shields.io/badge/Language-Java-blue.svg" />
  </a>
  <a href="http://guillaumelelouarn.fr/ulcoservice/javadoc/">
    <img alt="Javadoc" src="https://img.shields.io/badge/Documentation-Javadoc-red.svg" />
  </a>
</div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <div>
    <a href="https://gitlab.com/IsaacRuchalski/projet-tutore-fusion">
      <img src="images/logo.png" alt="Logo" width="80" height="80">
    </a>
  </div>
  <h3>ULCO Service</h3>
  <p>
    A useful daily tool at the UIT du Littoral Côte d'Opale.
    <br />
    <a href="http://guillaumelelouarn.fr/ulcoservice/javadoc"><strong>Documentation »</strong></a>
    <br />
    <a href="https://gitlab.com/IsaacRuchalski/projet-tutore-fusion/-/project_members">Project members</a>
    ·
    <a href="https://gitlab.com/IsaacRuchalski/projet-tutore-fusion/-/issues">Report bug</a>
    ·
    <a href="http://guillaumelelouarn.fr/ulcoservice/UML.svg">UML</a>
  </p>
  <p>
    03/31/21
    <br />
    Alpha release 1.0
    <br />
    <a href="mailto:ulco.us.release@gmail.com?subject=Alpha release access request">Request Google Play access</a>
  </p>
</div>

<!-- ABOUT -->
## About

ULCO Service is an Android application for all students and teachers of the IT department of the UIT du Littoral Côte d´Opale de Calais.

US offers to receive alerts when a change in schedule is recorded on ADE.

- Stay informed about schedule changes with notifications
- Access university services more quickly
- Save time on your planning

<p align="center" width="100%">
    <img width="32%" src="images/1.jpg"> 
    <img width="32%" src="images/2.jpg"> 
    <img width="32%" src="images/3.jpg"> 
</p>

<!-- FEATURES -->
## Features

3 Fragments : `News` `Services` and `Settings`

`News` : find the latest schedule changes and your bookmarked alerts.

`Services` : quick access to various university services on the web.

`Settings` : change school class and find your archived alerts.

3 actions : 
- `Swipe right` : bookmark
- `Swipe left` : archive
- `Long press` : delete

<!-- GUIDE -->
## Guide

The application is made up of a service that collects `ICS` flows and feeds the `RecyclerView` which format each piece of information, then inject it into each view of the fragment.

**What if I want to use ULCO Service with my school's ICS feed ?**

1. Make sure the ICS flow is in an open environment with trusted certificates

2. With the URL of the ICS you can use our `requestAgenda()` method in `backgroundService` class by adding a parameter like a String to the function to select which one you want to get

Example : `requestAgenda(“https://example.com”)` will get the ICS URL.

3. When getting the ics flow you will need to identify a significant item and how much there is

Example : `SUMMARY = 51`

Using some `regex` pattern, you can identify each of those items you need within the ICS flow response

Example : `SUMMARY:[a,b,c][a,b,c]//d//d`

This will identify each item corresponding to that pattern.
Combining this with a matcher will return each item in a String.

4. Using the number of items, we can loop through the ICS while seeking and getting information with pattern, matcher. We adding those information in an `ArrayList` and stock them into a new file with our multiple file function.

5. Finally, with some structural operation in the `RecyclerViewAdapter` you can adapt them to your format and display them in the Fragment View.

<!-- CONTRIBUTING -->
## Contributing
Contributions are what make the open source community such an amazing place. Any contributions you make are **greatly appreciated**.

Fork the project

Create your feature branch :
```sh
git checkout -b feature/MyFeature
```
Commit your changes :
```sh
git commit -m 'add MyFeature'
```
Push to the branch :
```sh
git push origin 'feature/MyFeature`
```
Open a pull request

<!-- CONTACT -->
## Contact
E-mail : ulco.us.release@gmail.com

GitLab project : [https://gitlab.com/IsaacRuchalski/projet-tutore-fusion](https://gitlab.com/IsaacRuchalski/projet-tutore-fusion)